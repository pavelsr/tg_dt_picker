## Update - message

```
$VAR1 = {
          'message' => {
                         'date' => 1476743992,
                         'message_id' => 1077,
                         'entities' => [
                                       {
                                         'length' => 5,
                                         'offset' => 0,
                                         'type' => 'bot_command'
                                       }
                                     ],
                         'chat' => {
                                     'type' => 'private',
                                     'id' => 218718957,
                                     'last_name' => 'Serikov',
                                     'username' => 'serikoff',
                                     'first_name' => 'Pavel'
                                   },
                         'text' => '/book',
                         'from' => {
                                   'last_name' => 'Serikov',
                                   'id' => 218718957,
                                   'username' => 'serikoff',
                                   'first_name' => 'Pavel'
                                 }
                       },
          'update_id' => 395658529
        };
```


 ## Update - inline


```
 $VAR1 = {
          'message' => {
                         'chat' => {}
                       },
          'update_id' => 395658532,
          'callback_query' => {
                              'chat_instance' => '-367321092722309930',
                              'id' => '939390767428928496',
                              'data' => 'CTC 3D printer',
                              'message' => {
                                           'text' => 'Please select an item',
                                           'date' => 1476744085,
                                           'message_id' => 1081,
                                           'from' => {
                                                     'username' => 'pavelsr_bot',
                                                     'id' => 222684756,
                                                     'first_name' => 'API Test Bot'
                                                   },
                                           'chat' => {
                                                     'username' => 'serikoff',
                                                     'id' => 218718957,
                                                     'last_name' => 'Serikov',
                                                     'type' => 'private',
                                                     'first_name' => 'Pavel'
                                                   }
                                         },
                              'from' => {
                                        'first_name' => 'Pavel',
                                        'id' => 218718957,
                                        'last_name' => 'Serikov',
                                        'username' => 'serikoff'
                                      }
                            }
        };
```

### Equal config options


```
{ "name": "time_range_select", 
  "parent": "day_select", 
  "welcome_msg": "Please select atime range", 
  "keyboard":
      [
        { "key": "morning", "answ" : "You are early bird" },
        { "key": "day", "answ" : "Good choice" },
        { "key": "evening", "answ" : "Owl" }
      ]
},

{ "name": "dynamic1", 
      "parent": "time_range_select", 
      "callback_msg": "morning", 
      "keyboard":
      [
        { "key": "key1"  },
        { "key": "key2" },
      ]
},

{ "name": "dynamic2", 
      "parent": "time_range_select", 
      "callback_msg": "day", 
      "keyboard":
      [
        { "key": "key3"  },
        { "key": "key4" },
      ]
    },
```

and

```
{ "name": "time_range_select", 
  "parent": "day_select", 
  "welcome_msg": "Please select atime range", 
  "keyboard":
      [
        { "key": "morning", "answ" : "You are early bird" },
        { "key": "day", "answ" : "Good choice" },
        { "key": "evening", "answ" : "Owl" }
      ]
},

{ "name": "dynamic2",
   "welcome_msg": "Please select atime range", 
   "parent": "time_range_select",
   "kb_build_func": "dynamic2_build_func"
},
```

Dynamic screens = screens that depends on user input

Can be defined as:

``callback_msg + keyboard``  
or 
``kb_build_func`` // in case of data-dependent state or something else
or 
``kb_build_func(some_variable)``
- in key when showed keyboard will depends on previous user input
kb_build_func is defined in a separate class and may have or may haven't additional parameters


### Features

#### Custom $k_per_row parameter for each screen

#### No validation for last screen



### For developers

#### Typical errors

```
Can't call method "build_keyboard_array" on unblessed reference
```

?


```
Can't call method "is_static" on an undefined value
```

Check that all class functions are called as $self->func





